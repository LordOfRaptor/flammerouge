package JeuPackage;

import CartePackage.CarteFatigue;
import CasePackage.Circuit;
import CyclistePackage.Cycliste;
import CyclistePackage.Rouleur;
import CyclistePackage.Sprinteur;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class JeuTest {

    @Test
    public void Test_joueurExistant() {
        Jeu j = new Jeu();
        assertEquals("Doit exister", true, j.joueurExistant("j1"));
        assertNotEquals("Ne doit pas exister", true, j.joueurExistant("j3"));
    }

    @Test
    public void Test_avanceligneDroite() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 0);
        j.avanceligneDroite(n1.sprinteur);
        assertEquals("Devrait avancer de 1 case", 1, j.getCircuit().getPosition(n1.sprinteur));
    }

    @Test
    public void Test_applicationAspiration() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 0);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 2);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 4);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 6);
        j.applicationAspiration();
        assertEquals("Ne devrait pas avoir bouger", 6, j.getCircuit().getPosition(n2.rouleur));
        assertEquals("Devrai avoir avancer d'une case", 5, j.getCircuit().getPosition(n2.sprinteur));
        assertEquals("Devrait avoir avancer de 2 cases", 4, j.getCircuit().getPosition(n1.rouleur));
        assertEquals("Devrait avoir avancer de 3 cases", 3, j.getCircuit().getPosition(n1.sprinteur));
    }

    @Test
    public void Test_applicationAspirationEnMontee() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 17);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 19);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 21);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 23);
        j.applicationAspiration();
        System.out.println(j.getCircuit().toString());
        assertEquals("Devrait avancer de 1 case", 23, j.getCircuit().getPosition(n2.rouleur));
        assertEquals("Devrait avancer de 1 case", 21, j.getCircuit().getPosition(n2.sprinteur));
        assertEquals("Devrait avancer de 1 case", 19, j.getCircuit().getPosition(n1.rouleur));
        assertEquals("Devrait avancer de 1 case", 17, j.getCircuit().getPosition(n1.sprinteur));
    }

    @Test
    public void Test_applicationAspirationSortieMontee() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 23);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 25);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 27);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 29);
        j.applicationAspiration();
        assertEquals("Devrait avancer de 1 case", 23, j.getCircuit().getPosition(n1.sprinteur));
        assertEquals("Devrait avancer de 1 case", 25, j.getCircuit().getPosition(n1.rouleur));
        assertEquals("Devrait avancer de 1 case", 28, j.getCircuit().getPosition(n2.sprinteur));
        assertEquals("Devrait avancer de 1 case", 29, j.getCircuit().getPosition(n2.rouleur));
    }

    @Test
    public void Test_applicationFatigue() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 0);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 2);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 4);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 5);
        assertEquals("Ne doit pas contenir de carte Fatigue", false, n1.sprinteur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Ne doit pas contenir de carte Fatigue", false, n1.rouleur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Ne doit pas contenir de carte Fatigue", false, n2.sprinteur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Ne doit pas contenir de carte Fatigue", false, n2.rouleur.getDefausse().contains(new CarteFatigue(2)));
        j.applicationFatigue();
        assertEquals("Doit contenir de carte Fatigue", true, n1.sprinteur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Doit contenir de carte Fatigue", true, n1.rouleur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Ne doit pas contenir de carte Fatigue", false, n2.sprinteur.getDefausse().contains(new CarteFatigue(2)));
        assertEquals("Doit contenir de carte Fatigue", true, n2.rouleur.getDefausse().contains(new CarteFatigue(2)));
    }

    @Test
    public void Test_appliqueMonte() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 8);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 11);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 18);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 26);
        assertEquals("Devrait faire 9 car pas de monte sur le chemin", 9, j.appliqueMonte(n1.sprinteur, 9));
        assertEquals("Devrait faire 6 car arrive sur une montee et devrait s'arreter avant la montee", 6, j.appliqueMonte(n1.rouleur, 7));
        assertEquals("Devrait faire 5 car commence commence sur une montee et finit sur une montee", 5, j.appliqueMonte(n2.sprinteur, 7));
        assertEquals("Devrait faire 5 car commence commence sur une montee et le fait qu'il arrive en dehors ne le fait pas changer", 5, j.appliqueMonte(n2.rouleur, 7));
    }

    @Test
    public void Test_appliqueDesc() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 26);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 29);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 32);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 0);
        assertEquals("Devrait faire 3 car ne commence pas par une descente", 3, j.appliqueDesc(n1.sprinteur, 3));
        assertEquals("Devrait faire 5 car commence sur une descente", 5, j.appliqueDesc(n1.rouleur, 3));
        assertEquals("Devrait faire 7 car superieur a 5", 7, j.appliqueMonte(n2.sprinteur, 7));

    }

    @Test
    public void Test_trie() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 34);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 29);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(0).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 0, 34);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 15);
        ArrayList<Cycliste> trie = j.trie();
        assertEquals("Devrait etre premier", trie.get(0), n1.sprinteur);
        assertEquals("Devrait etre premier", trie.get(1), n2.sprinteur);
        assertEquals("Devrait etre premier", trie.get(2), n1.rouleur);
        assertEquals("Devrait etre premier", trie.get(3), n2.rouleur);
    }

    @Test
    public void Test_premierCyc() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 34);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 29);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(0).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 0, 34);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 15);
        assertEquals("Devrait renvoyer le premier", n1.sprinteur, j.premierCyc());
    }

    @Test
    public void Test_faireAvancer() {
        Jeu j = new Jeu();
        Circuit c = j.getCircuit();
        Joueur n1 = j.getListeJoueurs().get(0);
        Joueur n2 = j.getListeJoueurs().get(1);
        n1.sprinteur = new Sprinteur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.sprinteur, 1, 32);
        n1.rouleur = new Rouleur(n1.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n1.rouleur, 1, 29);
        n2.sprinteur = new Sprinteur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.sprinteur, 1, 34);
        n2.rouleur = new Rouleur(n2.couleur, j.getCircuit().listeCase.get(1).get(0).getDroite());
        c.setCircuitCycliste(n2.rouleur, 1, 15);
        j.faireAvancer(n1.sprinteur, 2);
        j.faireAvancer(n1.rouleur, 5);
        assertEquals("Devrait a gauche sur la case 34", n1.sprinteur, j.getCircuit().listeCase.get(0).get(34).getCycliste());
        assertEquals("Devrait a droite sur la case 33 car toute la case 34 est occupe", n1.rouleur, j.getCircuit().listeCase.get(1).get(33).getCycliste());
        j.faireAvancer(n1.sprinteur, 1);
        assertEquals("Ne devrait pas voir bouger car il ne peut pas se deplacer de 1 car il se fait bloquer", n1.rouleur, j.getCircuit().listeCase.get(1).get(33).getCycliste());

    }


}