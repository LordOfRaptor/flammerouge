package JeuPackage;

import CartePackage.Carte;
import CartePackage.CarteFatigue;
import CasePackage.Circuit;
import CyclistePackage.Cycliste;
import CyclistePackage.Rouleur;
import CyclistePackage.Sprinteur;


import java.io.*;
import java.util.*;

/**
 * Classe permettant de creer le jeu avec le circuit, les cyclistes et les cartes
 *
 * @author quent
 * @version 1.0
 */
public class Jeu {

    /**
     * Liste des joueurs
     */
    private ArrayList<Joueur> listeJoueurs;

    /**
     * Circuit sur lequel les joueurs vont jouer
     */
    private Circuit circuit;

    /**
     * Nombre de joueurs dans la partie
     */
    private int nbJoueurs;

    /**
     * Fichier dans lequel va etre ecrit l'historique
     */
    private static BufferedWriter write;

    /**
     * Deck de fatigue des rouleurs
     */
    private static ArrayList<CarteFatigue> deckCarteFatigueRouleur = new ArrayList<>();

    /**
     * /**
     * Deck de fatigue des sprinteurs
     */
    private static ArrayList<CarteFatigue> deckCarteFatigueSprinteur = new ArrayList<>();

    /**
     * Liste des couleurs pour les equipes
     */
    private static ArrayList<String> listeCouleur = new ArrayList<>();

    /**
     * Cree un jeu avec un certains nombre de joueurs qui rempli
     * les decks carteFatigue et qui demande a l'utilisateurs
     * leurs noms et qui verifie l'existence d'un joueurs
     * puis les ajoutent a la partie, on lui demande ensuite
     * si ils veux creer son terrain ou en choisir un existant
     * @param nb nombre de joueur dans le jeu 
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public Jeu(int nb) throws ClassNotFoundException, IOException {
        remplir();
        this.circuit = new Circuit();
        this.nbJoueurs = nb;
        this.listeJoueurs = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < this.nbJoueurs; i++) {
            System.out.println("Rentrez un nom de joueur");
            String nom = sc.next();
            while (this.joueurExistant(nom)) {
                System.out.println("Nom deja pris, entrez un nouveau nom");
                nom = sc.next();
            }
            this.listeJoueurs.add(new Joueur(nom, listeCouleur.get(i), new Rouleur(listeCouleur.get(i), false)
                    , new Sprinteur(listeCouleur.get(i), false)));
        }
        this.circuit = Circuit.jeuCircuit(this.circuit);
    }

    /**
     * Initialisation d'un jeu basique a 2 joueurs
     * avec j1 et j2 et cree un circuit de base du jeu
     */
    public Jeu() {
        remplir();
        this.circuit = new Circuit();
        this.nbJoueurs = 2;
        this.listeJoueurs = new ArrayList<>();
        this.listeJoueurs.add(new Joueur("j1", "rouge", new Rouleur(("rouge"), false)
                , new Sprinteur(("rouge"), false)));
        this.listeJoueurs.add(new Joueur("j2", "bleu", new Rouleur(("bleu"), false)
                , new Sprinteur(("rouge"), false)));
        this.circuit.setCircuitMix();
    }

    /**
     * Remplits les decks de carte fatigue
     */
    private static void remplir() {
        for (int i = 0; i < 60; i++) {
            deckCarteFatigueRouleur.add(new CarteFatigue(2));
            deckCarteFatigueSprinteur.add(new CarteFatigue(2));
        }
    }

    /**
     * Demande le nombre de joueurs doit etre compris entre 2 et 4
     * @return le nombre de joueurs
     */
    private static int initJoueur() {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while (i < 2 || i > 4) {
            try {
                System.out.println("Rentrez le nombre de joueurs");
                i = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Pas un nombre : " + sc.next());
            }
        }
        return i;

    }

    /**
     * Verifie si un joueur existe
     * @param nom nom du joueur a verifier
     * @return true si le joueur existe
     */
    boolean joueurExistant(String nom) {
        for (Joueur j : this.listeJoueurs) {
            if (j.nomJoueur.equals(nom)) {
                return true;
            }
        }
        return false;
    }

    /**
     * fait avance de 1 case vers l'avant sans chengement de file
     * @param cy cycliste a faire avancer
     */
    void avanceligneDroite(Cycliste cy) {
        int file;
        if (cy.isFileDroite()) {
            file = 1;
        } else {
            file = 0;
        }
        int pos = this.circuit.getPosition(cy);
        this.circuit.setCircuitCycliste(null, file, pos);
        this.circuit.setCircuitCycliste(cy, file, pos + 1);
    }

    /**
     * Applique l'aspiration sur tous les cyclistes
     */
    void applicationAspiration() {
        ArrayList<Cycliste> listeCycliste = this.trie();
        Collections.reverse(listeCycliste);
        boolean fini = false;
        while (!(fini)) {
            fini = true;
            for (int i = 0; i < listeCycliste.size(); i++) {
                int posCyc = this.circuit.getPosition(listeCycliste.get(i));
                if (this.circuit.listeCase.get(1).get(posCyc + 2).getCycliste() != null && this.circuit.listeCase.get(1).get(posCyc + 1).getCycliste() == null
                        && this.circuit.listeCase.get(1).get(posCyc + 2).relief != 2 && this.circuit.listeCase.get(1).get(posCyc).relief != 2) {
                    this.avanceligneDroite(listeCycliste.get(i));
                    fini = false;
                }
            }
        }
    }

    /**
     * Ajoute des cartes fatigue aux  aux defausse des joueurs
     * qui n'ont aucun cycliste devant eux
     */
    void applicationFatigue() {
        ArrayList<Cycliste> lisCyc = this.trie();
        for (Cycliste c : lisCyc) {
            int pos = this.circuit.getPosition(c);
            if (this.circuit.listeCase.get(1).get(pos + 1).getCycliste() == null) {
                if (c instanceof Sprinteur) {
                    c.getDefausse().add(deckCarteFatigueSprinteur.get(0));
                    deckCarteFatigueSprinteur.remove(0);
                }
                if (c instanceof Rouleur) {
                    c.getDefausse().add(deckCarteFatigueRouleur.get(0));
                    deckCarteFatigueRouleur.remove(0);
                }
            }
        }
    }

    /**
     * Cree la liste de couleurs
     */
    private static void initCouleur() {
        listeCouleur.add("Rouge");
        listeCouleur.add("Bleu");
        listeCouleur.add("Vert");
        listeCouleur.add("Jaune");
    }


    /**
     * Methode faisant avancer tout les cyclistes en
     * fonction de leurs ordres le premier d'abord
     * Chaque joueurs pioche les cartes pour le cycliste choisi
     * une fois que les 4 joueurs ont choisi c'est au tour
     * du second cycliste de chaque joueur ensuite on fait avancer chaque
     * cycliste dans l'odre en lui appliquant les montee et les descentes
     */
    private void Tour() {
        Scanner sc = new Scanner(System.in);
        ArrayList<Cycliste> cycFinal = new ArrayList<>();
        ArrayList<Cycliste> tmp = new ArrayList<>();
        ArrayList<Carte> carteListe = new ArrayList<>();
        System.out.println(this.circuit.toString());
        for (int i = 0; i < this.nbJoueurs; i++) {
            System.out.println(this.listeJoueurs.get(i).nomJoueur + "(" + this.listeJoueurs.get(i).couleur + ")" + " choisissez qui du rouleur(R) ou du Sprinteur(S) " +
                    "vous voulez faire avancez");
            String s = sc.next();
            s = s.toUpperCase();
            while (!(s.equals("S") || s.equals("R"))) {
                System.out.println("S'il vous plait veuillez rentrez juste R ou S (minuscule ou majuscule)");
                s = sc.next();
                s = s.toUpperCase();
            }
            switch (s) {
                case "S":
                    tmp.add(this.listeJoueurs.get(i).rouleur);
                    carteListe.add(this.listeJoueurs.get(i).sprinteur.choixCarte());
                    cycFinal.add(this.listeJoueurs.get(i).sprinteur);
                    break;
                case "R":
                    tmp.add(this.listeJoueurs.get(i).sprinteur);
                    carteListe.add(this.listeJoueurs.get(i).rouleur.choixCarte());
                    cycFinal.add(this.listeJoueurs.get(i).rouleur);
                    break;
            }

        }
        for (int j = 0; j < this.nbJoueurs; j++) {
            System.out.println(this.listeJoueurs.get(j).nomJoueur + " vous allez joue votre 2eme cycliste ");
            carteListe.add(tmp.get(j).choixCarte());
            cycFinal.add(tmp.get(j));

        }
        tmp = this.trie();
        for (int k = 0; k < tmp.size(); k++) {
            int pos = cycFinal.indexOf(tmp.get(k));
            int val = carteListe.get(pos).getValeur();
            Cycliste cy = cycFinal.get(pos);
            val = this.appliqueDesc(cy, val);
            val = this.appliqueMonte(cy, val);
            val = this.appliqueVirage(cy, val);
            this.faireAvancer(cy, val);
        }
    }

    /**
     * Verifie si le cycliste est sur une montee ou va
     * passer sur une montee et renvoie la nouvelle valeur
     * de deplacement du cycliste
     * @param cy cycliste qu'on verifie
     * @param val valeur initiale de depalcement
     * @return nouvelle valeur apres verification si il n'y
     *          a pas eu de changement val est retourner
     */
    public int appliqueMonte(Cycliste cy, int val) {
        int pos = this.circuit.getPosition(cy);
        if (val <= 5) {
            return val;
        }
        if (this.circuit.listeCase.get(1).get(pos).relief == 2) {
            return 5;
        }
        boolean montee = false;
        for (int i = pos; i <= (pos + val); i++) {
            if (this.circuit.listeCase.get(1).get(i).relief == 2) {
                montee = true;
                if (i - pos > 5) {
                    return i - pos - 1;
                }
            }
        }
        if (montee) {
            return 5;
        } else {
            return val;
        }
    }
    
    
    /**
     * Verifie si le cycliste est sur un virage, ou si il va se deplacer sur un virage
     * 	si le cycliste est sur un virage il a une chance de tomber et alors de reculer d'une case
     * 	si il avance sur deux virage ou plus il a plus de chance de tomber
     * 	enfin un sprinter a plus de chance de tomber q'un rouleur 
     * 
     * @param cy cycliste qu'on verifie
     * @param val valeur initiale de depalcement
     * @return nouvelle valeur apres verification si il n'y
     *          a pas eu de changement val est retourner
     */
    public int appliqueVirage(Cycliste cy, int val) {
    	int pos = this.circuit.getPosition(cy);
    	int compt=0;
    	double chute;
    	for(int i=pos+1;i<(pos+val);i++) {
    		if (this.circuit.listeCase.get(1).get(i).type == 1) {
            	compt++;
            }
    	}
    	if(compt>=1 && compt<3) {
    		if(cy instanceof  Rouleur) {
    			chute=(double)Math.random()*1;
    			if(chute>0.7) {
                    System.out.println(cy.toString() + " a glisse dans un virage");
    				val--;
    			}
    		}else {
    			chute=(double)Math.random()*1;
    			if(chute>0.6) {
                    System.out.println(cy.toString() + " a glisse dans un virage");
    				val--;
    			}
    		}
    	}else if(compt>=3) {
    		if(cy instanceof  Rouleur) {
    		    chute=(double)Math.random()*1;
    			if(chute>0.6) {
                    System.out.println(cy.toString() + " a glisse dans un virage");
    				val--;
    			}
    		}else {
    			chute=(double)Math.random()*1;
    			if(chute>0.5) {
                    System.out.println(cy.toString() + " a glisse dans un virage");
    				val--;
    			}
    		}
    	}
    	return val;
    }

    /**
     * Verfifie si le cycliste est sur une decente ou pas
     * et renvoie la nouvelle valeur de deplacement du cycliste
     * @param cy cycliste qu'on verifie
     * @param val valeur initiale de depalcement
     * @return nouvelle valeur apres verification si il n'y
     *          a pas eu de changement val est retourner
     */
    int appliqueDesc(Cycliste cy, int val) {
        int pos = this.circuit.getPosition(cy);
        if (this.circuit.listeCase.get(1).get(pos).relief == 1 && val <= 5) {
            return 5;
        } else {
            return val;
        }
    }

    /**
     * Trie les cyclistes du premier au dernier en
     * tenant compte de la file
     * @return la liste trier
     */
    ArrayList<Cycliste> trie() {
        ArrayList<Cycliste> listecyc = new ArrayList<>();
        for (Joueur j : this.listeJoueurs) {
            listecyc.add(j.rouleur);
            listecyc.add(j.sprinteur);
        }
        Comparator cmp = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Cycliste c1 = (Cycliste) o1;
                Cycliste c2 = (Cycliste) o2;
                if (circuit.getPosition(c2) == circuit.getPosition(c1)) {
                    if (c1.isFileDroite()) {
                        return circuit.getPosition(c2) - circuit.getPosition(c1) - 1;
                    } else {
                        return circuit.getPosition(c2) - circuit.getPosition(c1) + 1;
                    }
                } else {
                    return circuit.getPosition(c2) - circuit.getPosition(c1);
                }
            }
        };
        Collections.sort(listecyc, cmp);
        return listecyc;
    }

    /**
     * Selectionne le premier cycliste
     * @return le cycliste en tete
     */
    public Cycliste premierCyc() {
        ArrayList<Cycliste> cycListe = this.trie();
        return cycListe.get(0);
    }

    /**
     * verifie si le cycliste en tete est sur une
     * case d'arrive
     * @return true si la partie est fini
     */
    public boolean fini() {
        int index = this.circuit.getPosition(this.premierCyc());
        return (this.circuit.listeCase.get(1).get(index).type == 3);

    }

    /**
     * Fait avancer un cycliste d'un certain nombre de case
     * en cherchant une place libre en partant de l'endroit ou il doit arriver
     * en regardant sur la case de droite puis celle de gauche et recule
     * d'une case a droit puis sur celle de gauche etc..
     * et s'arrete des qu'il trouve une position
     * @param cy cycliste a faire avancer
     * @param val de combien il doit avancer
     */
    void faireAvancer(Cycliste cy, int val) {
        int file;
        if (cy.isFileDroite()) {
            file = 1;
        } else {
            file = 0;
        }
        int pos = this.circuit.getPosition(cy);
        this.circuit.setCircuitCycliste(null, file, pos);
        int i = pos + val;
        if (i > this.circuit.listeCase.get(0).size()) {
            i = this.circuit.listeCase.get(0).size() - 1;
        }
        int j = 1;
        while (i >= pos && this.circuit.listeCase.get(j).get(i).getCycliste() != null) {

            while (j > 0 && this.circuit.listeCase.get(j).get(i).getCycliste() != null) {
                j--;
            }
            if (this.circuit.listeCase.get(j).get(i).getCycliste() != null) {
                j = 1;
                i--;
            }
        }
        this.circuit.setCircuitCycliste(cy, j, i);
        boolean droite;
        if (j == 1) {
            droite = true;
        } else {
            droite = false;
        }
        cy.setFileDroite(droite);
    }

    /**
     * Affiche les case de depart d'un circuit
     * avec presence de cycliste ou non et les places
     * disponibles
     */
    void afficherDepart() {
        Cycliste c;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 5; j++) {
                c = this.circuit.listeCase.get(i).get(j).getCycliste();
                if (c == null) {
                    System.out.print("[ 0" + (i * 5 + j) + " ]");
                } else {
                    System.out.print("[ " + c.getType().substring(0, 1) + c.getCouleur().substring(0, 1) + " ]");
                }
            }
            System.out.println("");
        }
    }

    /**
     * renvoie le circuit courant
     * @return circuit courant
     */
    public Circuit getCircuit() {
        return circuit;
    }

    /**
     * renvoie la liste des joueurs courant
     * @return les joueurs courant
     */
    public ArrayList<Joueur> getListeJoueurs() {
        return listeJoueurs;
    }

    /**
     * Place les joueurs en fonction de leurs nombre
     * et verifie que les places cont libres au debut un cycliste peut
     * etre initialiser a gauche au debut car cela ne change rien lors du
     * premier tour et un joueur peut vouloir laisser la case droite
     * disponible pour un autre joueur
     */
    public void placementJoueur() {
        Scanner sc = new Scanner(System.in);
        int place;
        boolean pasValide;
        for (Joueur j : this.listeJoueurs) {
            this.afficherDepart();
            pasValide = true;
            while (pasValide) {
                try {
                    System.out.println("Rentrer la place pour le sprinteur");
                    place = sc.nextInt();
                    while (place < 0 || place > 9 || this.circuit.listeCase.get(place / 5).get(place % 5).getCycliste() != null) {
                        System.out.println("Place deja prise ou en dehors du plateau\nRentrez une nouvelle valeur");
                        place = sc.nextInt();
                    }
                    j.sprinteur = new Sprinteur(j.couleur, this.circuit.listeCase.get(place / 5).get(0).getDroite());
                    this.circuit.setCircuitCycliste(j.sprinteur, place / 5, place % 5);
                    pasValide = false;
                } catch (InputMismatchException e) {
                    System.out.println("Pas un chiffre : " + sc.next());
                }
            }
            pasValide = true;
            this.afficherDepart();
            while (pasValide) {
                try {
                    System.out.println("Rentrer la place pour le rouleur");
                    place = sc.nextInt();
                    while (place < 0 || place > 9 || this.circuit.listeCase.get(place / 5).get(place % 5).getCycliste() != null) {
                        System.out.println("Place deja prise ou en dehors du plateau\nRentrez une nouvelle valeur");
                        place = sc.nextInt();
                    }
                    j.rouleur = new Rouleur(j.couleur, this.circuit.listeCase.get(place / 5).get(0).getDroite());
                    this.circuit.setCircuitCycliste(j.rouleur, place / 5, place % 5);
                    pasValide = false;
                } catch (InputMismatchException e) {
                    System.out.println("Pas un chiffre : " + sc.next());
                }
            }
        }
    }

    /**
     * Cree un nouveua fichier historique en fonction du nombre de
     * fichier deja present dans l'historique et le stock dans
     * l'attribut static writer
     * @throws IOException
     */
    private static void ouvertureFich() throws IOException {
        File f = new File("src\\Historique");
        int compteur = f.list().length;

        write = new BufferedWriter(new FileWriter("src\\Historique\\Historique" + compteur + ".txt"));
    }

    /**
     * Sauvegarde a chaque etape le terrain den le fichier
     * historique ouvert
     * ATTENTION : si la partie n'est pas terminer le fichier sera vide
     * car il se close lors de la fin du jeu et sauvegarde a ce moment
     * @throws IOException
     */
    public void saveHistor() throws IOException {
        String s = this.circuit.toString();
        String[] separated = s.split("\\n");
        for (String ss : separated) {
            write.write(ss);
            write.newLine();
        }
        write.newLine();
    }

    /**
     * Ferme le fichier write
     * @throws IOException
     */
    private static void closeFich() throws IOException {
        write.close();
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        boolean ouvert = true;
        try {
            ouvertureFich();
        } catch (IOException e) {
            System.out.println("impossible d'ouvrir ou de creer le fichier");
            ouvert = false;
            e.printStackTrace();
        }
        int nb = initJoueur();
        initCouleur();
        Jeu j = new Jeu(nb);
        j.placementJoueur();
        if (ouvert) {
            try {
                j.saveHistor();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        while (!(j.fini())) {
            j.Tour();
            j.applicationAspiration();
            j.applicationFatigue();
            if (ouvert) {
                try {
                    j.saveHistor();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (ouvert) {
            try {
                closeFich();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Le premier est : " + j.premierCyc());


    }
}
