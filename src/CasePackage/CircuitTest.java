package CasePackage;
import static org.junit.Assert.*;

import org.junit.Test;

import CyclistePackage.Cycliste;
import CyclistePackage.Sprinteur;
import JeuPackage.Joueur;

public class CircuitTest {

	
	@Test
	public void testsetCircuit() {
		Circuit c = new Circuit();
		c.setCircuit();
		assertEquals("premiere case ligne droite plate","Ligne droite plate",c.getNomCase(0,7));
		assertEquals("virage plat ","virage plat",c.getNomCase(0,32));
	}
	
	
	@Test
	public void testsetCircuitRelief() {
		Circuit c = new Circuit();
		c.setCircuitRelief();
		assertEquals("premiere case Ligne droite descente","Ligne droite descente",c.getNomCase(0,11));
	}
	
	
	@Test
	public void testsetCircuitMix() {
		Circuit c = new Circuit();
		c.setCircuitMix();
		assertEquals("premiere case virage descente","virage descente",c.getNomCase(0,13));
	}
	
	
	@Test
	public void testsetCircuitArrive() {
		Circuit c = new Circuit();
		c.setCircuit();
		assertEquals("premiere case arrive","arrive",c.getNomCase(0,77));
	}
	
	
	@Test
	public void testsetCircuitDepart() {
		Circuit c = new Circuit();
		c.setCircuit();
		assertEquals("premiere case depart","depart",c.getNomCase(0,0));
	}

	
	@Test
	public void testCote() {
		Circuit c = new Circuit();
		c.setCircuit();
		Cycliste cycli = new Sprinteur("Rouge",false);
		c.setCircuitCycliste(cycli,0,0);
		assertEquals("le cycliste doit etre sur la premiere case",true,c.isEstsur(0,0));
		assertEquals("le cycliste doit etre sur la premiere case de gauche",false,c.listeCase.get(0).get(0).getDroite());
	}
	
	
	@Test
	public void testLigneDroite(){
		Circuit c = new Circuit();
		c.ligneDroite(1, 1, true);
		assertEquals("premiere case ligne droite plate","Ligne droite plate",c.getNomCase(1,0));
	}
	
	
	@Test
	public void testVirage(){
		Circuit c = new Circuit();
		c.virage(1, 1, true);
		assertEquals("premiere case virage plat","virage plat",c.getNomCase(1,0));
	}
	
	
	@Test
	public void testsetCircuitCycliste(){
		Circuit c = new Circuit();
		c.virage(1, 1, true);
		assertEquals("premiere case virage plat","virage plat",c.getNomCase(1,0));
	}
	
	
	@Test
	public void testIsEstSur(){
		Circuit c = new Circuit();
		c.setCircuit();
		assertEquals("il ne devrait pas avoir de cycliste sur la case",false,c.isEstsur(0,0));
	}
}
